//
//  KPRelateCell.h
//  K@pook Photo
//
//  Created by Tharin Nilsri on 11/26/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPRelateCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *cellUserPic;
@property (nonatomic, strong) IBOutlet UILabel *cellSubject;
@property (nonatomic, strong) IBOutlet UILabel *cellDescription;
@property (nonatomic, strong) IBOutlet UILabel *cellUsername;

@end
