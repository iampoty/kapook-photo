//
//  KPPhotoViewCell.h
//  K@pook Photo
//
//  Created by Tharin Nilsri on 10/30/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import "PSCollectionViewCell.h"
#import "UIImageView+WebCache.h"
@interface KPPhotoViewCell : PSCollectionViewCell{
    UIImageView *imageThumb;
    UILabel *lableTitle;
    
    UIView *userView;
    UILabel *usernameLabel;
    UILabel *dateLabel;
    UIImageView *userPicture;
    
    
    UIView *optionsView;
    UIImageView *optionView;
    UIImageView *optionComm;
    UILabel *optionViewLabel;
    UILabel *optionCommLabel;

}

+ (CGFloat)heightForViewWithObject:(id)object inColumnWidth:(CGFloat)columnWidth;

@end
