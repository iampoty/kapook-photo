//
//  KPPhotoCollectionViewCell.h
//  K@pook Photo
//
//  Created by Tharin Nilsri on 11/18/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KPPhotoCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UIImageView *thumb;

@end
