//
//  KPPhotoCollectionView.m
//  K@pook Photo
//
//  Created by Tharin Nilsri on 11/18/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import "KPPhotoCollectionView.h"
#import "AFNetworking.h"
#import "KPPhotoCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "KPRelateCell.h"
#import "Manage.h"
#import <FacebookSDK/FacebookSDK.h>

@interface KPPhotoCollectionView (){
    UIActivityIndicatorView *activityIndicator;
}
@end

typedef void (^MyAppBlock)(void);

@implementation KPPhotoCollectionView

@synthesize info;
@synthesize relatesData;
@synthesize contentArray;
@synthesize relatesView;
@synthesize collectionView;
@synthesize subjectLabel;
@synthesize jsonData;
@synthesize listPhotos;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSLog(@"CollectionView  > %s:%d",__FUNCTION__,__LINE__);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        self.collectionView.contentSize=CGSizeMake(668, 550);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    float h = 50;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
    }
    
    self.subjectLabel.text = @"";
    self.subjectLabel.font = [UIFont boldSystemFontOfSize:18.0];
    self.subjectLabel.numberOfLines = 0;
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame=CGRectMake(0.0, 0.0, 50.0, 50.0);
    activityIndicator.transform = CGAffineTransformMakeScale(2.5, 2.5);
    activityIndicator.center = self.view.center;
    [self.view addSubview: activityIndicator];
    [activityIndicator startAnimating];
    
    self.loginview.readPermissions = @[@"basic_info"];
    self.collectionView.backgroundColor=[UIColor whiteColor];
    [self loadDataSource];
    
    NSString *cellNib=@"";
    if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPhone){
        cellNib=@"KPRelateCell~iPhone";
    }else{
        cellNib=@"KPRelateCell~iPad";
    }
    [self.relatesView registerNib:[UINib nibWithNibName:cellNib bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"Cell"];
}

-(void) loadDataSource{
//    [self.jsonData removeAllObjects];
    self.tapRefreshBtn.hidden=YES;
    self.subjectLabel.hidden=YES;
    self.userinfoView.hidden=YES;
    self.relatesView.hidden=YES;
    self.loginview.hidden=YES;
    
    self.contentArray =nil;
    self.contentArray = [[NSArray alloc] init];
    
    self.listPhotos =nil;
    self.listPhotos = [[NSMutableArray alloc] init];

    self.jsonData = nil;
    self.jsonData = [[NSMutableArray alloc] init];

    self.relatesData=nil;
    self.relatesData=[[NSMutableArray alloc] init];
    
    self.userLabel.text=@"";
    self.viewsLabel.text=@"0";
    self.postdateLabel.text=@"";

    self.subjectLabel.text = [info objectForKey:@"subject"];
    if([info objectForKey:@"member"] !=[NSNull null]){
        NSDictionary *member = [info objectForKey:@"member"];
        NSURL *userpictrueUrl = [[NSURL alloc] initWithString:[member objectForKey:@"picture"]];
        [self.userPicture  setImageWithURL:userpictrueUrl];
        
        if([member objectForKey:@"username"] !=[NSNull null]){
            self.userLabel.text = [member objectForKey:@"username"];
        }else{
            
        }
    }

    if ([info objectForKey:@"views"] != [NSNull null] && [info objectForKey:@"views"]!=nil) {
        self.viewsLabel.text=[NSString stringWithFormat:@"%@",[info objectForKey:@"views"]];
    }
    
    if ([info objectForKey:@"date_create"] != [NSNull null] && [info objectForKey:@"date_create"]!=nil) {
        self.postdateLabel.text = [NSString stringWithFormat:@"โพสเมื่อ %@",[info objectForKey:@"date_create"]];
    }
    
    
    
    NSString *u;
    u = [NSString stringWithFormat:@"http://kapi.kapook.com/world/content/get_content/%@",[info objectForKey:@"_id"]];
    NSLog(@"url %@",u);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:u parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        self.jsonData = [responseObject objectForKey:@"contents"];
        self.relatesData=[responseObject objectForKey:@"relates"];
        
        NSArray *photo = [responseObject objectForKey:@"contents"];
        self.contentArray = [[NSArray alloc] initWithObjects:photo, nil];
        
        [self dataSourceDidLoad];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self dataSourceDidError] ;
    }];
}

-(void)dataSourceDidLoad{
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    
    [self.collectionView registerClass:[KPPhotoCollectionViewCell class] forCellWithReuseIdentifier:@"kpCell"];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPhone){
        [flowLayout setItemSize:CGSizeMake(150 , 150)];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    }else{
        [flowLayout setItemSize:CGSizeMake(150, 150)];
        [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    }
    
//    NSLog(@"self.contentArray.count:%lu",(unsigned long)self.contentArray.count);
    
    MWPhoto *photo;
    NSMutableArray *data = [self.contentArray objectAtIndex:0];
//    NSLog(@"data.count:%lu",(unsigned long)data.count);
    for (int i = 0; i<data.count; i++) {
        NSDictionary *cellData = [data objectAtIndex:i];
        photo =[MWPhoto photoWithURL:[NSURL URLWithString:[cellData objectForKey:@"source"] ]];
        if([self.info objectForKey:@"description"] !=[NSNull null]){
            photo.caption = [self.info objectForKey:@"description"];
        }else{
            photo.caption =@"";
        }
        [self.listPhotos addObject:photo];
    }

    self.subjectLabel.hidden=NO;
    self.userinfoView.hidden=NO;
    self.relatesView.hidden=NO;
    self.loginview.hidden=NO;
    
    
    [self.collectionView setCollectionViewLayout:flowLayout];
    [self.collectionView reloadData];
    [activityIndicator stopAnimating];
    [self.relatesView reloadData];
}

-(void)dataSourceDidError{
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    [activityIndicator stopAnimating];
    
    self.tapRefreshBtn.hidden=NO;
}

//-(void)viewWillAppear:(BOOL)animated {
//    
//    [super viewWillAppear:animated];
//    
//    
//}

-(IBAction)reloadDataSource{
    self.relatesData=nil;
    self.listPhotos=nil;
    [self.relatesView reloadData];
    [self.collectionView reloadData];
    [activityIndicator startAnimating];
    self.tapRefreshBtn.hidden=YES;
    [self loadDataSource];
}

/******** Relate Table View *********/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    self.info=[self.relatesData objectAtIndex:indexPath.row];
//    NSArray* subviews = [[NSArray alloc] initWithArray: self.collectionView.subviews];
//    //    NSLog(@"subviews:%d",[subviews count]);
//    for(UIImageView *view in subviews){
//        if ([view isKindOfClass:[UIImageView class]]) {
//            [view removeFromSuperview];
//        }
//    }
    [self reloadDataSource];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    return self.relatesData.count;
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    CGSize itemSize = CGSizeMake(80, 80);
//    UIGraphicsBeginImageContext(itemSize);
//    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
////    [cell.cellUserPic.image drawInRect:imageRect];
////    cell.cellUserPic.image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    KPRelateCell *cell;
    if(cell==nil){
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    SDWebImageDownloader *manager = [SDWebImageManager sharedManager].imageDownloader;
    [manager setValue:@"http://www.kapook.com" forHTTPHeaderField:@"Referer"];

    NSDictionary *obj=[self.relatesData objectAtIndex:indexPath.row];
    NSURL *url = [NSURL URLWithString: [obj objectForKey:@"thumb"]];
    
    CGSize itemSize = CGSizeMake(80, 80);
    UIGraphicsBeginImageContext(itemSize);
    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
    [cell.cellUserPic.image drawInRect:imageRect];
    cell.cellUserPic.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [cell.cellUserPic setImageWithURL:url placeholderImage:[UIImage imageNamed:@"icon_No-Pic.jpg"] options:SDWebImageRefreshCached];
    
    if([obj objectForKey:@"subject"] !=[NSNull null]){
        cell.cellSubject.text=[obj objectForKey:@"subject"];
    }else{
        cell.cellSubject.text=@"";
    }

//    if([obj objectForKey:@"description"] !=[NSNull null]){
//        cell.cellDescription.text=[obj objectForKey:@"description"];
//    }else{
        cell.cellDescription.text=@"";
//    }
    
    if([obj objectForKey:@"member"] !=[NSNull null]){
        NSDictionary *member = [obj objectForKey:@"member"];
        if([member objectForKey:@"username"] !=[NSNull null]){
            cell.cellUsername.text=[member objectForKey:@"username"];
        }else{
            cell.cellUsername.text=@"";
        }
    }else{
        cell.cellUsername.text=@"";
    }

    
    
//    cell.backgroundView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:bg]];
//    cell.textLabel.font = [UIFont boldSystemFontOfSize:12.0];
//    cell.backgroundView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cells_bg.png"]];
//    cell.selectedBackgroundView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cells_bg.png"]];
    return cell;
}
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    SDWebImageDownloader *manager = [SDWebImageManager sharedManager].imageDownloader;
//    [manager setValue:@"http://www.kapook.com" forHTTPHeaderField:@"Referer"];
//
//    NSDictionary *obj=[self.relatesData objectAtIndex:indexPath.row];
//    NSURL *url = [NSURL URLWithString: [obj objectForKey:@"thumb"]];
//    
//    UITableViewCell *cell = [[UITableViewCell alloc] init];
//    cell.textLabel.text=[obj objectForKey:@"subject"];
//    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
//    [cell.imageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"icon_No-Pic.jpg"] options:SDWebImageRefreshCached];
//    CGSize itemSize = CGSizeMake(80, 80);
//    UIGraphicsBeginImageContext(itemSize);
//    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
//    [cell.imageView.image drawInRect:imageRect];
//    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    cell.backgroundView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cells_bg.png"]];
//    cell.selectedBackgroundView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cells_bg.png"]];

//    static NSString *CellIdentifier = @"mycell";
//    KPRelateCell *cell = (KPRelateCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    if (cell == nil) {
//        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"KPRelateCell" owner:self options:nil];
//        for (id currentObject in topLevelObjects) {
//            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
//                cell = (KPRelateCell *)currentObject;
//                break;
//            }
//        }
//    }
//    
//    return cell;
//}


//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSInteger row=indexPath.row;
//    NSString *bg;
//    if(row==0){
//        NSLog(@"row top: %d",0);
//        bg=@"cell_top.png";
//    }else if(row==self.relatesData.count-1){
//        NSLog(@"row buttom: %ld",(unsigned long)self.relatesData.count);
//        bg=@"cell_buttom.png";
//    }else{
//        NSLog(@"row: normal");
//        bg=@"cell_middle.png";
//    }
//    cell.backgroundView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:bg]];
//    cell.textLabel.font = [UIFont boldSystemFontOfSize:12.0];
//    cell.backgroundView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cells_bg.png"]];
//    cell.selectedBackgroundView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cells_bg.png"]];
    
//    CGSize itemSize = CGSizeMake(80, 80);
//    UIGraphicsBeginImageContext(itemSize);
//    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
//    [cell.imageView.image drawInRect:imageRect];
//    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();

//}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    NSString *sectionName;
//    sectionName=@"เนื้อหาที่เกี่ยวข้อง";
////    sectionName=@"";
//    return sectionName;
//}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    CGFloat height = 0.0;
//    cell.imageView.frame = CGRectMake(0,0,32,32);
    return 95;
}
/******** Relate Table View *********/


/********* Collection View ***********/
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
//    NSLog(@"%s:%d > %lu",__FUNCTION__,__LINE__,self.jsonData.count);
//    return self.jsonData.count;
    
//    NSLog(@"%s:%d > %lu",__FUNCTION__,__LINE__,self.contentArray.count);
    return self.contentArray.count;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    NSMutableArray *sectionArray = [self.jsonData objectAtIndex:section];
    NSMutableArray *sectionArray = [self.contentArray objectAtIndex:section];
    
//    NSLog(@"%s:%d > %lu",__FUNCTION__,__LINE__,(unsigned long)[sectionArray count]);
    return [sectionArray count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    static NSString *cellIdentifier = @"kpCell";
    KPPhotoCollectionViewCell *cell = (KPPhotoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
//    NSDictionary *data = [self.jsonData objectAtIndex:indexPath.section];
    NSMutableArray *data = [self.contentArray objectAtIndex:indexPath.section];
    NSDictionary *cellData = [data objectAtIndex:indexPath.row];
    NSURL *thumbUrl = [NSURL URLWithString: @""];
    thumbUrl=[[NSURL alloc] initWithString:[cellData objectForKey:@"thumb"]];
    
    SDWebImageDownloader *manager = [SDWebImageManager sharedManager].imageDownloader;
    [manager setValue:@"http://www.kapook.com" forHTTPHeaderField:@"Referer"];
    
    [cell.thumb setImageWithURL:thumbUrl placeholderImage:[UIImage imageNamed:@"icon_No-Pic.jpg"] options:SDWebImageRefreshCached];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton=YES;
    browser.displayNavArrows=NO;
    browser.wantsFullScreenLayout=YES;
    browser.zoomPhotosToFill=YES;
    [browser setCurrentPhotoIndex:indexPath.row];
//    Modal
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:nc animated:YES completion:nil];

}

/********* Collection View ***********/


/********* MWPhotoBrowser ********/
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.listPhotos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.listPhotos.count)
        return [self.listPhotos objectAtIndex:index];
    return nil;
}
/********* MWPhotoBrowser ********/


/****** Facebook ******/


- (IBAction)Share:(id)sender {
    NSURL* url = [NSURL URLWithString:[info objectForKey:@"link"]];
    //    NSLog(@"url %@",url);
    NSLog(@"%s:%d>%@",__FUNCTION__,__LINE__,url);
    
    [FBDialogs presentShareDialogWithLink:url
                                  handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                      if(error) {
                                          NSLog(@"Error: %@", error.description);
                                      } else {
                                          NSLog(@"%s:%d:Success",__FUNCTION__,__LINE__);
                                      }
                                  }];
    
    //    FBAppCall *call =  [FBDialogs presentShareDialogWithLink:url
    //                                                     handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
    //                                                         if(error) {
    //                                                             NSLog(@"Error: %@", error.description);
    //                                                         } else {
    //                                                             NSLog(@"Success!");
    //                                                         }
    //                                                         NSLog(@"results:%@",results);
    //                                                     }];
}

- (BOOL)requestPermissionsWithCompletion:(MyAppBlock)completion {
    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    if (!FBSession.activeSession.isOpen) {
        [[[UIAlertView alloc]
          initWithTitle:@""
          message:@"Please log in with Facebook to share."
          delegate:nil
          cancelButtonTitle:@"OK"
          otherButtonTitles:nil, nil] show];
        return true;
    } else if ([FBSession.activeSession.permissions
                indexOfObject:@"publish_actions"] == NSNotFound) {
        [FBSession.activeSession
         requestNewPublishPermissions:@[@"publish_actions"]
         defaultAudience:FBSessionDefaultAudienceEveryone
         completionHandler:^(FBSession *session, NSError *error) {
             if (!error) {
                 // Permissions granted. Call the completion method
                 completion();
             } else {
                 NSLog(@"Error: %@", error.description);
             }
         }];
        return true;
    } else {
        return false;
    }
}

- (void)showAlert:(NSString *) alertMsg {
    if (![alertMsg isEqualToString:@""]) {
        [[[UIAlertView alloc] initWithTitle:@"Result"
                                    message:alertMsg
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}


- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    /** Navigation Button **/
//    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Share Facebook" style:UIBarButtonItemStylePlain target:self action:@selector(Share:)];
//    self.navigationItem.rightBarButtonItem = anotherButton;
    /** Navigation Button **/

    if(FBSession.activeSession.isOpen && !userid)
    {
        NSLog(@"%s:%d",__FUNCTION__,__LINE__);
        [FBRequestConnection
         startForMeWithCompletionHandler:^(FBRequestConnection *connection, id <FBGraphUser>user, NSError *error) {
             NSLog(@"user.id:%@",user.id);
             [self checkLoginKapook:user.id];
             [self addBtn];
         }];
    }else{
        NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    }
    
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    self.navigationItem.rightBarButtonItems = nil;
}
/****** Facebook ******/




/******** Kapook ********/

-(void)addBtn{
    
    UIBarButtonItem *btnAddtoKapook=[[UIBarButtonItem alloc] initWithTitle:@"Add to Kapook" style:UIBarButtonItemStylePlain target:self action:@selector(addtokapookBtn:)];
    UIBarButtonItem *btnShareFB=[[UIBarButtonItem alloc] initWithTitle:@"FB Share" style:UIBarButtonItemStylePlain target:self action:@selector(Share:)];
    
    NSArray *myButtonArray=[[NSArray alloc] initWithObjects:btnAddtoKapook,btnShareFB, nil];

    self.navigationItem.rightBarButtonItems = myButtonArray;
}

-(IBAction)addtokapookBtn:(id)sender{
    NSLog(@"addtokapookBtn");
    [[Manage sharedManager] AddtoKapook: info initWithView:(UIView *)self.navigationController.view];
}

//-(IBAction)fbshareBtn:(id)sender{
//    NSLog(@"fbshareBtn");
//    [self Share:<#(id)#>];
//}


-(void)checkLoginKapook:(NSString *)idfacebook
{
//    NSString *u = [NSString stringWithFormat:@"http://kapi.kapook.com/world/content/get_content/%@",[info objectForKey:@"id"]];
    NSString *u = [NSString stringWithFormat:@"http://signup-demo.kapook.com/api/facebook/%@?type=json",idfacebook];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:u parameters:nil success:^(AFHTTPRequestOperation *operation, id jsonObject) {
        NSLog(@"%s:%d : Write User.plist.",__FUNCTION__,__LINE__);
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"User.plist"];
        
        BOOL success;
        success = [fileManager fileExistsAtPath:filePath];
        
        if (!success) {
            
            NSLog(@"Test-Info.plist successfully copied to DocumentsDirectory.");
        }
        
        NSMutableArray *plistArray = [[NSMutableArray alloc] init];
        
        NSMutableDictionary *profile = [NSMutableDictionary dictionary];
        
        [profile setValue:[jsonObject objectForKey:@"id"] forKey:@"id"];
        [profile setValue:[jsonObject objectForKey:@"name"] forKey:@"name"];
        [profile setValue:[jsonObject objectForKey:@"userid"] forKey:@"userid"];
        [profile setValue:[jsonObject objectForKey:@"username"] forKey:@"username"];
        [profile setValue:[jsonObject objectForKey:@"picture"] forKey:@"picture"];
        [profile setValue:idfacebook forKey:@"facebookid"];
        
        [plistArray addObject:profile];
        [plistArray writeToFile:filePath atomically:YES];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}
/******** Kapook ********/

@end
