//
//  KPPhotoViewCell.m
//  K@pook Photo
//
//  Created by Tharin Nilsri on 10/30/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import "KPPhotoViewCell.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>

#define MARGIN 8.0

@implementation KPPhotoViewCell


- (id)initWithFrame:(CGRect)frame
{
//    NSLog(@"%s / %d",__FUNCTION__,__LINE__);
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor lightGrayColor];

        lableTitle=[[UILabel alloc] initWithFrame:CGRectZero];
        lableTitle.font = [UIFont boldSystemFontOfSize:18.0];
        lableTitle.clipsToBounds = YES;
        lableTitle.numberOfLines = 0;
        [self addSubview:lableTitle];

        imageThumb = [[UIImageView alloc] initWithFrame:CGRectZero];
        imageThumb.clipsToBounds = YES;
        [self addSubview:imageThumb];
        
        /*
         UIView *userView;
         UILabel *usernameLabel;
         UILabel *dateLabel;
         UIImageView *userPicture;
         
         
         UIView *optionsView;
         UIImageView *optionView;
         UIImageView *optionComm;
         UILabel *optionViewLabel;
         UILabel *optionCommLabel;
         */
        
        /*** User info ***/
        userView = [[UIImageView alloc] initWithFrame:CGRectZero];
        userView.clipsToBounds = YES;
        [self addSubview:userView];
        
        userPicture = [[UIImageView alloc] initWithFrame:CGRectZero];
        userPicture.clipsToBounds = YES;
        [userView addSubview:userPicture];
        
        usernameLabel=[[UILabel alloc] initWithFrame:CGRectZero];
        usernameLabel.font = [UIFont boldSystemFontOfSize:18.0];
        usernameLabel.numberOfLines = 0;
        [userView addSubview:usernameLabel];
        
        dateLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        dateLabel.font = [UIFont boldSystemFontOfSize:12.0];
        dateLabel.numberOfLines = 1;
        [userView addSubview:dateLabel];
        
        /*** User info ***/
        
        /***** viewOption *****/
        optionsView =[[UIView alloc] initWithFrame:CGRectZero];
        optionsView.clipsToBounds=YES;
        
        optionView=[[UIImageView alloc] initWithFrame:CGRectZero];
        optionView.clipsToBounds=YES;
        [optionsView addSubview:optionView];
        
        optionComm=[[UIImageView alloc] initWithFrame:CGRectZero];
        optionComm.clipsToBounds=YES;
        [optionsView addSubview:optionComm];
        
        optionViewLabel=[[UILabel alloc] initWithFrame:CGRectZero];
        optionViewLabel.font =[UIFont boldSystemFontOfSize:14.0];
        optionViewLabel.numberOfLines=1;
        [optionsView addSubview:optionViewLabel];
        
        optionCommLabel=[[UILabel alloc] initWithFrame:CGRectZero];
        optionCommLabel.font =[UIFont boldSystemFontOfSize:14.0];
        optionCommLabel.numberOfLines=1;
        [optionsView addSubview:optionCommLabel];
        
        [self addSubview:optionsView];
        /***** viewOption *****/
        
        
        /** Corner Radius **/
//        self.layer.masksToBounds = YES;
//        self.layer.borderWidth = 1.0f;
//        self.layer.cornerRadius = 6.0f;
//        self.layer.borderColor= [[UIColor colorWithRed:207.0f/255.0f green:207.0f/255.0f blue:207.0f/255.0f alpha:1] CGColor];
    }
    return self;
}

+ (CGFloat)heightForViewWithObject:(id)object inColumnWidth:(CGFloat)columnWidth {
//    NSLog(@"%s / %d",__FUNCTION__,__LINE__);
    
    CGFloat height = 0.0;
    CGFloat width = columnWidth - MARGIN * 2;
    
    height += MARGIN;
    
    
    // Image
    CGFloat objectWidth =220.0;
    CGFloat objectHeight = 165.0;
    if ([object objectForKey:@"thumb"] != [NSNull null]) {
        NSDictionary *thumb=[object objectForKey:@"thumb"];
        if ([thumb objectForKey:@"home"] != [NSNull null] && [thumb objectForKey:@"home"]!=nil) {
            NSDictionary *thumbhome=[thumb objectForKey:@"home"];
            if ([thumbhome objectForKey:@"width"] != [NSNull null]) {
                objectWidth=[[thumbhome objectForKey:@"width"] floatValue];
            }
            if ([thumbhome objectForKey:@"height"] != [NSNull null]) {
                objectHeight=[[thumbhome objectForKey:@"height"] floatValue];
            }
        }else if ([thumb objectForKey:@"thumb"] != [NSNull null] && [thumb objectForKey:@"thumb"] != nil) {
            NSDictionary *thumbthumb=[thumb objectForKey:@"thumb"];
            if ([thumbthumb objectForKey:@"width"] != [NSNull null]) {
                objectWidth=[[thumbthumb objectForKey:@"width"] floatValue];
            }
            if ([thumbthumb objectForKey:@"height"] != [NSNull null]) {
                objectHeight=[[thumbthumb objectForKey:@"height"] floatValue];
            }
        }
    }

//    NSLog(@"init thumb success.");
    CGFloat scaledHeight = floorf(objectHeight / (objectWidth / width));
    height += scaledHeight;
    
    // Label
    NSString *title = @"";
    
    if([object objectForKey:@"description"] !=[NSNull null]){
        title=[object objectForKey:@"description"];
    }
//    NSLog(@"init description success.");
    CGSize labelSize = CGSizeZero;
    UIFont *labelFont = [UIFont boldSystemFontOfSize:18.0];
    labelSize = [title sizeWithFont:labelFont constrainedToSize:CGSizeMake(width, INT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    height += labelSize.height;
    
    height +=50;//UserView Height
    height +=40;//OptionsView Height
    
    height += MARGIN+MARGIN+MARGIN;//Space title & OptionsView , UserView
//    NSLog(@"height:%f",height);
    return  height;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    imageThumb.image = nil;
    lableTitle.text=nil;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat width = self.frame.size.width - MARGIN * 2;
    CGFloat top = MARGIN;
    CGFloat left = MARGIN;

    CGFloat objectWidth = 0.0;
    CGFloat objectHeight = 0.0;
    
    userView.frame = CGRectMake(0, 0, width+MARGIN+MARGIN, 50+MARGIN+MARGIN);    
    
    userPicture.frame = CGRectMake(MARGIN, MARGIN, 48, 48);
    
    usernameLabel.frame = CGRectMake(55+MARGIN, MARGIN, 150, 50);
    usernameLabel.backgroundColor = [UIColor lightGrayColor];
    usernameLabel.textColor = [UIColor whiteColor];
    [usernameLabel sizeToFit];

    
    dateLabel.frame = CGRectMake(55+MARGIN, 35, 150, 50);
    dateLabel.backgroundColor = [UIColor lightGrayColor];
    dateLabel.textColor = [UIColor whiteColor];
    [dateLabel sizeToFit];

    
    
    top+=userView.frame.size.height;
    
    if ([self.object objectForKey:@"thumb"] != [NSNull null]) {
        NSDictionary *thumb=[self.object objectForKey:@"thumb"];
        if ([thumb objectForKey:@"home"] != [NSNull null] && [thumb objectForKey:@"home"]!=nil) {
            NSDictionary *thumbhome=[thumb objectForKey:@"home"];
            if ([thumbhome objectForKey:@"width"] != [NSNull null]) {
                objectWidth=[[thumbhome objectForKey:@"width"] floatValue];
            }
            if ([thumbhome objectForKey:@"height"] != [NSNull null]) {
                objectHeight=[[thumbhome objectForKey:@"height"] floatValue];
            }
        }else if ([thumb objectForKey:@"thumb"] != [NSNull null] && [thumb objectForKey:@"thumb"] != nil) {
            NSDictionary *thumbthumb=[thumb objectForKey:@"thumb"];
            if ([thumbthumb objectForKey:@"width"] != [NSNull null]) {
                objectWidth=[[thumbthumb objectForKey:@"width"] floatValue];
            }
            if ([thumbthumb objectForKey:@"height"] != [NSNull null]) {
                objectHeight=[[thumbthumb objectForKey:@"height"] floatValue];
            }
        }
    }
    CGFloat scaledHeight = floorf(objectHeight / (objectWidth / width));
    
    imageThumb.frame = CGRectMake(left, top, width, scaledHeight);
    
    CGSize labelSize = CGSizeZero;
    labelSize = [lableTitle.text sizeWithFont:lableTitle.font constrainedToSize:CGSizeMake(width, INT_MAX) lineBreakMode:lableTitle.lineBreakMode];
    
    top+=imageThumb.frame.size.height;
    lableTitle.frame = CGRectMake(left, top, labelSize.width, labelSize.height);
//    lableTitle.frame = CGRectMake(left, imageThumb.frame.origin.y + imageThumb.frame.size.height + MARGIN, labelSize.width, labelSize.height);
    lableTitle.backgroundColor=[UIColor lightGrayColor];

    top+=lableTitle.frame.size.height+MARGIN;
    
    optionsView.frame=CGRectMake(0, top, width+MARGIN+MARGIN, 40);
    optionsView.backgroundColor=[UIColor blackColor];
    
    
    top=(optionsView.frame.size.height-15)/2;
    [optionView setImage:[UIImage imageNamed:@"icon_view_view.png"]];
    optionView.frame = CGRectMake(MARGIN, top, 20, 15);
    optionViewLabel.frame=CGRectMake(optionView.frame.origin.x+optionView.frame.size.width+MARGIN, top, 50, 15);
    optionViewLabel.textColor=[UIColor whiteColor];
    optionViewLabel.backgroundColor=[UIColor blackColor];
    [optionViewLabel sizeToFit];
    
    [optionComm setImage:[UIImage imageNamed:@"icon_view_view2.png"]];
    optionComm.frame = CGRectMake(optionViewLabel.frame.origin.x+optionViewLabel.frame.size.width+MARGIN+10, top, 20, 15);
    optionCommLabel.frame=CGRectMake(optionComm.frame.origin.x+optionComm.frame.size.width+MARGIN, top, 50, 15);
    optionCommLabel.textColor=[UIColor whiteColor];
    optionCommLabel.backgroundColor=[UIColor blackColor];
    [optionCommLabel sizeToFit];

    
}

-(void)collectionView:(PSCollectionView *)collectionView fillCellWithObject:(id)object atIndex:(NSInteger)index{
    [super collectionView:collectionView fillCellWithObject:object atIndex:index];
    
    NSURL *thumbUrl=nil;
    if ([[object objectAtIndex:index] objectForKey:@"thumb"] != [NSNull null]) {
        NSDictionary *thumb=[[object objectAtIndex:index] objectForKey:@"thumb"];
        if ([thumb objectForKey:@"home"] != [NSNull null] && [thumb objectForKey:@"home"]!=nil) {
            NSDictionary *thumbhome=[thumb objectForKey:@"home"];
            if ([thumbhome objectForKey:@"src"] != [NSNull null]) {
                thumbUrl=[[NSURL alloc] initWithString:[thumbhome objectForKey:@"src"]];
            }
            
        }else if ([thumb objectForKey:@"thumb"] != [NSNull null] && [thumb objectForKey:@"thumb"] != nil) {
            NSDictionary *thumbthumb=[thumb objectForKey:@"thumb"];
            if ([thumbthumb objectForKey:@"src"] != [NSNull null]) {
                thumbUrl=[[NSURL alloc] initWithString:[thumbthumb objectForKey:@"src"]];
            }
        }
    }
    
    if(thumbUrl!=nil){
        SDWebImageDownloader *manager = [SDWebImageManager sharedManager].imageDownloader;
        [manager setValue:@"http://www.kapook.com" forHTTPHeaderField:@"Referer"];
        
        [imageThumb setImageWithURL:thumbUrl placeholderImage:[UIImage imageNamed:@"icon_No-Pic.jpg"] options:SDWebImageRefreshCached];
    }
    
    
    NSString *title=@"";
    
    if([[object objectAtIndex:index] objectForKey:@"description"] !=[NSNull null]){
        title=[[object objectAtIndex:index] objectForKey:@"description"];
    }
    lableTitle.text = title;

    if ([[object objectAtIndex:index]  objectForKey:@"views"] != [NSNull null] && [[object objectAtIndex:index]  objectForKey:@"views"]!=nil) {
        optionViewLabel.text=[NSString stringWithFormat:@"%@",[[object objectAtIndex:index]  objectForKey:@"views"]];
    }
    if ([[object objectAtIndex:index]  objectForKey:@"commentcount"] != [NSNull null] && [[object objectAtIndex:index]  objectForKey:@"commentcount"]!=nil) {
        optionCommLabel.text=[NSString stringWithFormat:@"%@",[[object objectAtIndex:index]  objectForKey:@"commentcount"]];
    }


    NSURL *userPictureUrl=nil;
    NSString *userName=@"";
    
    if ([[object objectAtIndex:index] objectForKey:@"member"] != [NSNull null]) {
        NSDictionary *member = [[object objectAtIndex:index] objectForKey:@"member"];
        
        if ([member objectForKey:@"picture"] != [NSNull null]) {
            userPictureUrl = [[NSURL alloc] initWithString:[member objectForKey:@"picture"]];
        }
        if ([member objectForKey:@"username"] != [NSNull null]) {
            userName=[member objectForKey:@"username"];
        }
    }
    if(userPictureUrl!=nil){
        SDWebImageDownloader *manager = [SDWebImageManager sharedManager].imageDownloader;
        [manager setValue:@"http://www.kapook.com" forHTTPHeaderField:@"Referer"];
        
        [userPicture setImageWithURL:userPictureUrl placeholderImage:[UIImage imageNamed:@"user_nopic.png"] options:SDWebImageRefreshCached];
    }
    
    if(![userName isEqual:@""]){
        usernameLabel.text=userName;
    }
    if([[object objectAtIndex:index] objectForKey:@"date_create"] != [NSNull null]){
        dateLabel.text=[NSString stringWithFormat:@"โพสเมื่อ %@",[[object objectAtIndex:index] objectForKey:@"date_create"]];
    }
    
}

@end
