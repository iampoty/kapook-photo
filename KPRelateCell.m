//
//  KPRelateCell.m
//  K@pook Photo
//
//  Created by Tharin Nilsri on 11/26/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import "KPRelateCell.h"

@implementation KPRelateCell

@synthesize cellUserPic;
@synthesize cellSubject;
@synthesize cellDescription;
@synthesize cellUsername;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.cellSubject.text=nil;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
//NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    // Configure the view for the selected state
}

//-(void)setBackgroundView:(UIView *)backgroundView{
//
//}

@end
