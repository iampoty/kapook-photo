//
//  KPPhotoCollectionViewLayout~iPad.m
//  K@pook Photo
//
//  Created by Tharin Nilsri on 11/18/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import "KPPhotoCollectionViewLayout_iPad.h"

@implementation KPPhotoCollectionViewLayout_iPad
-(id)init
{
    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    
    if (!(self = [super init])) return nil;
    self.itemSize = CGSizeMake(200, 200);
    self.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    self.minimumInteritemSpacing = 10.0f;
    self.minimumLineSpacing = 10.0f;
    
    return self;
}
@end
