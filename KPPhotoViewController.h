//
//  KPPhotoViewController.h
//  K@pook Photo
//
//  Created by Tharin Nilsri on 11/4/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSCollectionView.h"
#import "KPAppDelegate.h"
#import "MWPhotoBrowser.h"

@interface KPPhotoViewController : UIViewController <MWPhotoBrowserDelegate,UITableViewDataSource,UITableViewDelegate>{
    IBOutlet UIScrollView *leftView;
    IBOutlet UITableView *relatesView;
    IBOutlet UILabel *subjectLabel;
    NSDictionary *info;
}

//@property (nonatomic,retain) IBOutlet UILabel *tapRefresh;
@property (nonatomic,retain) IBOutlet UIButton *tapRefreshBtn;

@property (nonatomic, strong) NSMutableArray *listPhotos;
@property (nonatomic, retain) NSMutableArray *jsonData;
@property (nonatomic, retain) NSMutableArray *relatesData;
@property (nonatomic,strong) IBOutlet UILabel *subjectLabel;
@property (nonatomic,strong) IBOutlet UIScrollView *leftView;
@property (nonatomic,strong) IBOutlet UIView *photosView;
@property (nonatomic,strong) IBOutlet UIView *rightView;
@property (weak, nonatomic) IBOutlet FBLoginView *loginview;
@property (nonatomic,strong) IBOutlet UITableView *relatesView;
@property (strong, nonatomic) NSDictionary *info;

@property (nonatomic,strong) IBOutlet UIView *userinfoView;
@property (nonatomic,strong) IBOutlet UIImageView *userPicture;
@property (nonatomic,strong) IBOutlet UILabel *userLabel;
@property (nonatomic,strong) IBOutlet UILabel *viewsLabel;
@property (nonatomic,strong) IBOutlet UILabel *postdateLabel;
@end
