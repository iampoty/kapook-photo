//
//  KPPhotoCollectionViewCell.m
//  K@pook Photo
//
//  Created by Tharin Nilsri on 11/18/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import "KPPhotoCollectionViewCell.h"

@implementation KPPhotoCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSArray *arrayOfViews=[[NSBundle mainBundle] loadNibNamed:@"KPPhotoCollectionViewCell~iPad" owner:self options:nil];
        if([arrayOfViews count] <1){
            return nil;
        }
        
        if(![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]){
            return nil;
        }
        
        self=[arrayOfViews objectAtIndex:0];
    }
    return self;
}

@end
