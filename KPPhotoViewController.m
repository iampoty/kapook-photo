//
//  KPPhotoViewController.m
//  K@pook Photo
//
//  Created by Tharin Nilsri on 11/4/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import "KPPhotoViewController.h"
#import "AFNetworking.h"
#import "UIImageView+WebCache.h"
#import <FacebookSDK/FacebookSDK.h>

@interface KPPhotoViewController (){
    UIActivityIndicatorView *activityIndicator;
}

@end

typedef void (^MyAppBlock)(void);

@implementation KPPhotoViewController
@synthesize info;
@synthesize leftView;
@synthesize subjectLabel;
@synthesize relatesData;
@synthesize relatesView;
@synthesize jsonData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    NSLog(@"%s:%d:%@",__FUNCTION__,__LINE__,info);
    
	self.subjectLabel.text = @"";
    self.subjectLabel.font = [UIFont boldSystemFontOfSize:18.0];
    self.subjectLabel.numberOfLines = 0;
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame=CGRectMake(0.0, 0.0, 50.0, 50.0);
        activityIndicator.transform = CGAffineTransformMakeScale(2.5, 2.5);
//    activityIndicator.center = self.view.center;
//    activityIndicator.center = self.photosView.center;
    activityIndicator.center = self.leftView.center;
    [self.leftView addSubview: activityIndicator];
//    [self.photosView addSubview: activityIndicator];
    [activityIndicator startAnimating];
    
    [self loadJson];
    
    self.loginview.readPermissions = @[@"basic_info"];
    
    /*

    self.userinfoView.layer.borderWidth = 1.0f;
    self.userinfoView.layer.cornerRadius =10.0f;
    self.userinfoView.layer.borderColor = [[UIColor colorWithRed:207.0f/255.0f green:207.0f/255.0f blue:207.0f/255.0f alpha:1] CGColor];
     */    
    [self.viewsLabel sizeToFit];
    
//    if([info objectForKey:@"member"] !=[NSNull null]){
//        NSDictionary *member = [info objectForKey:@"member"];
//        NSURL *userpictrueUrl = [[NSURL alloc] initWithString:[member objectForKey:@"picture"]];
//        [self.userPicture  setImageWithURL:userpictrueUrl];
//        self.userLabel.text = [member objectForKey:@"username"];
//    }
//    
//    if ([info objectForKey:@"views"] != [NSNull null] && [info objectForKey:@"views"]!=nil) {
//        self.viewsLabel.text=[NSString stringWithFormat:@"%@",[info objectForKey:@"views"]];
//    }
//
//    self.postdateLabel.text = [NSString stringWithFormat:@"โพสเมื่อ %@",[info objectForKey:@"date_create"]];
}


-(void) loadJson{
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
//    [self.jsonData removeAllObjects];
    self.relatesView.hidden=YES;
    
    self.jsonData = nil;
    self.jsonData = [[NSMutableArray alloc] init];
    
//    [self.relatesData removeAllObjects];
    self.relatesData=nil;
    self.relatesData=[[NSMutableArray alloc] init];
    
    self.userLabel.text=@"";
    self.viewsLabel.text=@"0";
    self.postdateLabel.text=@"";
    
    self.subjectLabel.text = [info objectForKey:@"subject"];
    if([info objectForKey:@"member"] !=[NSNull null]){
        NSDictionary *member = [info objectForKey:@"member"];
        NSURL *userpictrueUrl = [[NSURL alloc] initWithString:[member objectForKey:@"picture"]];
        [self.userPicture  setImageWithURL:userpictrueUrl];
        self.userLabel.text = [member objectForKey:@"username"];
    }

    if ([info objectForKey:@"views"] != [NSNull null] && [info objectForKey:@"views"]!=nil) {
        self.viewsLabel.text=[NSString stringWithFormat:@"%@",[info objectForKey:@"views"]];
    }
    
    if ([info objectForKey:@"date_create"] != [NSNull null] && [info objectForKey:@"date_create"]!=nil) {
        self.postdateLabel.text = [NSString stringWithFormat:@"โพสเมื่อ %@",[info objectForKey:@"date_create"]];
    }
    
    
    NSString *u;
    u = [NSString stringWithFormat:@"http://kapi.kapook.com/world/content/get_content/%@",[info objectForKey:@"_id"]];
    NSLog(@"url %@",u);

    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:u parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        self.jsonData = [responseObject objectForKey:@"contents"];
        self.relatesData=[responseObject objectForKey:@"relates"];
        [self dataSourceDidLoad];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self dataSourceDidError] ;
    }];
    }

-(void)dataSourceDidLoad {
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    UIImage *placeholder=[UIImage imageNamed:@"icon_No-Pic.jpg"];
    CGFloat width = 142;
    CGFloat height = 142;
    NSInteger contentSize=668;
    if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPhone){
        width=130;
        height=130;
        contentSize=320;
    }
    
    self.userinfoView.hidden=YES;
    CGFloat margin = 20;
    CGFloat startX = 20;
    CGFloat startY = 20;
    CGFloat browsWidth=self.leftView.frame.size.width;
    NSInteger browsCols=4;
//    startX=20;
//    startY=20;
    browsCols=browsWidth/(width+margin);

//    NSLog(@"browsWidth: %f",browsWidth);
//    NSLog(@"width: %f",width);
//    NSLog(@"margin: %f",margin);
    UIScrollView *scrollview=(UIScrollView *) self.leftView;
    NSInteger count=self.jsonData.count;
//    NSLog(@"count: %d",count);
//    NSLog(@"browsCols: %d",browsCols);
    
    
    NSInteger totalRow=count/browsCols;
    CGFloat totalHeight=1024;//(height+margin*(self.jsonData.count/browsCols))
    totalHeight=(height+margin)*(totalRow+2)-margin-margin;
//    scrollview.contentSize=CGSizeMake(contentSize, totalHeight);
    scrollview.showsHorizontalScrollIndicator=NO;
    scrollview.showsVerticalScrollIndicator=YES;
    
    CGFloat fsize=0.0;
    //TODO check height of frame
    
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    MWPhoto *photo;
    for (int i = 0; i<self.jsonData.count; i++) {
        NSURL *url=[self.jsonData[i] objectForKey:@"source"];
        photo =[MWPhoto photoWithURL:[NSURL URLWithString:[self.jsonData[i] objectForKey:@"source"] ]];
        NSString *description=@"";
        if([self.info objectForKey:@"description"] !=[NSNull null]){
            description=[self.info objectForKey:@"description"];
        }
        photo.caption = description;
        [photos addObject:photo];
        
        UIImageView *imageView = [[UIImageView alloc] init];
//        [scrollview addSubview:imageView];
        int row = i/browsCols;
        int column = i%browsCols;
        CGFloat x = startX + column * (width + margin);
        CGFloat y = startY + row * (height + margin);
//        NSLog(@"x:%f , y:%f",x,y);
        imageView.frame = CGRectMake(x, y, width, height);
        [imageView setImageWithURL:url placeholderImage:placeholder];
        imageView.tag = i;
        imageView.userInteractionEnabled = YES;
        [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImage:)]];
        imageView.clipsToBounds = YES;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [self.leftView addSubview:imageView];
        
//        fsize=imageView.frame.origin.y+imageView.frame.size.height+margin;
//        NSLog(@"y:%f, height:%f",y,height);
        fsize=y+height+margin;
        
//        scrollview.contentSize=CGSizeMake(contentSize, fsize+margin);
    }

    
//    NSLog(@"fsize:%f",fsize);
//    fsize+=margin;
//    NSLog(@"fsize:%f",fsize);
    
    
    
//    NSLog(@"self.photosView.frame 1: %f,%f",self.photosView.frame.size.width,self.photosView.frame.size.height);
//    CGRect tmpSize=self.photosView.frame;
//    tmpSize.size=CGSizeMake(scrollview.contentSize.width, fsize);
//    self.photosView.frame=tmpSize;
//    self.photosView.backgroundColor=[UIColor redColor];
//    CGSize tmpSize=CGSizeMake(scrollview.contentSize.width, fsize);
//    [self.photosView.frame  tmpSize];
//    NSLog(@"self.photosView.frame 2: %f,%f",self.photosView.frame.size.width,self.photosView.frame.size.height);

//    NSLog(@"self.leftView.frame.size.width: %f",self.leftView.frame.size.width);
    
    
//    CGRect contentRect = CGRectZero;
//    CGFloat scrollViewHeight = 0.0f;
//    for (UIView* view in self.leftView.subviews)
//    {
//        contentRect = CGRectUnion(contentRect, view.frame);
//        scrollViewHeight += view.frame.size.height;
//        NSLog(@"All scrollview subvie > %f",view.frame.size.height);
//    }
//    NSLog(@"scrollViewHeight = %f",scrollViewHeight);
//    self.leftView.contentSize = contentRect.size;
//    [self.leftView setContentSize:(CGSizeMake(320, scrollViewHeight))];

//    NSLog(@"self.userinfoView.frame : %f",self.userinfoView.frame.size.height);
//    self.photosView.frame=CGRectMake(0,0,contentSize, fsize);
    if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPhone){
        self.userinfoView.frame=CGRectMake(20, fsize, 280, 100);
        fsize+=100+margin;
        self.relatesView.frame=CGRectMake(20, fsize, 280, 440);
        fsize+=440+margin+margin+margin;
    }else{
        fsize+=100;
    }

    
    self.leftView.contentSize=CGSizeMake(self.leftView.frame.size.width,fsize);
    
//    scrollview.contentSize=CGSizeMake(contentSize, fsize);
//    self.leftView=scrollview;
    
//    NSLog(@"scrollview %f",scrollview.contentSize.height);
//    NSLog(@"self.leftView %f",self.leftView.contentSize.height);
    self.userinfoView.hidden=NO;
    
//    CGRect photoView=self.photosView.frame; // setFrame:CGRectMake(startX, startX, 320.0, fsize)];
//    photoView.size=CGSizeMake(scrollview.contentSize.width, scrollview.contentSize.height);
//    self.photosView.frame=photoView;
    
    self.listPhotos=photos;
    
//    NSLog(@"self.relatesData:%@",self.relatesData);
    
//    UIScrollView *relatesView=[[UIScrollView alloc] init];
    
//    startX=20;
//    startY=20;
//    width=50;
//    height=50;
//    fsize=0;
//    CGFloat x=startX;
//    CGFloat y=startY;
//    for (int i = 0; i<self.relatesData.count; i++) {
//        relatesData
//        NSURL *url=[self.relatesData[i] objectForKey:@"thumb"];
//        NSString *subject = [self.relatesData[i] objectForKey:@"subject"];
//        NSLog(@"%f,%f",x,y);
//        NSLog(@"subject: %@",subject);
//        NSLog(@"url %@",url);
//        UIImageView *imageView = [[UIImageView alloc] init];
//        imageView.frame = CGRectMake(x, y, width, height);
//        [imageView setImageWithURL:url placeholderImage:placeholder];
//        imageView.userInteractionEnabled = YES;
//        imageView.clipsToBounds = YES;
//        imageView.contentMode = UIViewContentModeScaleAspectFill;
//        [self.relatesView addSubview:imageView];
//        UILabel *lableSubject=[[UILabel alloc] initWithFrame:CGRectZero];
//        lableSubject.frame = CGRectMake(x+width+margin, y, 345-width-margin-margin, 70);
//        lableSubject.font = [UIFont boldSystemFontOfSize:14.0];
//        lableSubject.clipsToBounds = YES;
//        lableSubject.numberOfLines = 0;
//        lableSubject.text=subject;
//        [self.relatesView addSubview:lableSubject];
//        y += margin+90;
//        fsize=y;
//    }
//    relatesView.backgroundColor=[UIColor lightGrayColor];
//    NSLog(@"relatesView:%f,%f",relatesView.frame.size.width,relatesView.frame.size.height);
//    
//    for (UIImageView *imgView in imgBall) {
//        [imgView removeFromSuperview];
//    }
//    [imgBall removeAllObjects];
    self.relatesView.hidden=NO;
    [self.relatesView reloadData];
    [activityIndicator stopAnimating];
}

-(void)dataSourceDidError {
//    NSLog(@"dataSourceDidError");
    [activityIndicator stopAnimating];
    self.tapRefreshBtn.hidden=NO;
//    [self.photosView bringSubviewToFront:self.tapRefreshBtn];
    
}

-(void)tapImage:(UITapGestureRecognizer *)tap{
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    CFIndex i=tap.view.tag;
    // Create browser
	MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton=YES;
    browser.displayNavArrows=NO;
    browser.wantsFullScreenLayout=YES;
    browser.zoomPhotosToFill=YES;
    [browser setCurrentPhotoIndex:i];
    
//    Modal
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:nc animated:YES completion:nil];
    
    
//    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
//    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
  //  [self presentViewController:nc animated:YES completion:nil];
//    [self.navigationController pushViewController:browser animated:YES];
//    Modal
    
//    Push
//    [self.navigationController pushViewController:browser animated:YES];
//    Push
    
    /** End MWPhotoBrowser **/
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    return self.listPhotos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    if (index < self.listPhotos.count)
        return [self.listPhotos objectAtIndex:index];
    return nil;
}


/****** Facebook ******/


- (IBAction)Share:(id)sender {
    NSURL* url = [NSURL URLWithString:[info objectForKey:@"link"]];
//    NSLog(@"url %@",url);
    NSLog(@"%s:%d>%@",__FUNCTION__,__LINE__,url);
    
    [FBDialogs presentShareDialogWithLink:url
      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
          if(error) {
              NSLog(@"Error: %@", error.description);
          } else {
              NSLog(@"%s:%d:Success",__FUNCTION__,__LINE__);
          }
      }];
    
//    FBAppCall *call =  [FBDialogs presentShareDialogWithLink:url
//                                                     handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
//                                                         if(error) {
//                                                             NSLog(@"Error: %@", error.description);
//                                                         } else {
//                                                             NSLog(@"Success!");
//                                                         }
//                                                         NSLog(@"results:%@",results);
//                                                     }];
}

- (BOOL)requestPermissionsWithCompletion:(MyAppBlock)completion {
    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    if (!FBSession.activeSession.isOpen) {
        [[[UIAlertView alloc]
          initWithTitle:@""
          message:@"Please log in with Facebook to share."
          delegate:nil
          cancelButtonTitle:@"OK"
          otherButtonTitles:nil, nil] show];
        return true;
    } else if ([FBSession.activeSession.permissions
                indexOfObject:@"publish_actions"] == NSNotFound) {
        [FBSession.activeSession
         requestNewPublishPermissions:@[@"publish_actions"]
         defaultAudience:FBSessionDefaultAudienceEveryone
         completionHandler:^(FBSession *session, NSError *error) {
             if (!error) {
                 // Permissions granted. Call the completion method
                 completion();
             } else {
                 NSLog(@"Error: %@", error.description);
             }
         }];
        return true;
    } else {
        return false;
    }
}

- (void)showAlert:(NSString *) alertMsg {
    if (![alertMsg isEqualToString:@""]) {
        [[[UIAlertView alloc] initWithTitle:@"Result"
                                    message:alertMsg
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}


- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Share Facebook" style:UIBarButtonItemStylePlain
                                                                     target:self action:@selector(Share:)];
    self.navigationItem.rightBarButtonItem = anotherButton;
    //    self.publishButton.hidden = NO;
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    self.navigationItem.rightBarButtonItem = nil;
}
/****** Facebook ******/

-(IBAction)reloadDataSource{
    [activityIndicator startAnimating];
    
    if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPhone){
        self.tapRefreshBtn.hidden=YES;
        self.userinfoView.hidden=YES;
    }
    [self.leftView sendSubviewToBack:self.tapRefreshBtn];
//    [self.photosView sendSubviewToBack:self.tapRefreshBtn];
    [self loadJson];
    
//    [self.scrollView scrollRectToVisible:myView.frame animated:YES];
    [self.leftView scrollRectToVisible:self.view.frame animated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
//    NSLog(@"%s:%d leftView contentsize 1 %f, %f",__FUNCTION__,__LINE__,self.leftView.contentSize.width, self.leftView.contentSize.height);
//    NSLog(@"%s:%d leftView 1 %f, %f",__FUNCTION__,__LINE__,self.leftView.frame.size.width,self.leftView.frame.size.height);
//    NSLog(@"viewDidAppear leftView 2 %f, %f",self.leftView.contentSize.width, self.leftView.contentSize.height);
//    NSLog(@"viewDidAppear photosView %f, %f",self.photosView.frame.size.width, self.photosView.frame.size.height);
//    NSLog(@"%s:%d photosView %f,%f",__FUNCTION__,__LINE__,self.photosView.frame.size.width,self.photosView.frame.size.height);
}



/******** Relate Table View *********/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//  Reload info
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
//    [activityIndicator startAnimating];
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
//    self.tapRefreshBtn.hidden=YES;
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
//    [self.leftView sendSubviewToBack:self.tapRefreshBtn];
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    
    
//    KPPhotoViewController *p=nil;
//    if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPhone){
//        p =[[KPPhotoViewController alloc] initWithNibName:@"KPPhotoViewController~iPhone" bundle:nil];
//    }else{
//        p =[[KPPhotoViewController alloc] initWithNibName:@"KPPhotoViewController~iPad" bundle:nil];
//    }
//    p.info = [self.listPhotos objectAtIndex:index];

//    NSDictionary *p=nil;
//    [p setValue:nil forKey:@"date_create"];
    
    self.info=[self.relatesData objectAtIndex:indexPath.row];
    
//    [self.jsonData removeAllObjects];
//    self.relatesView.reloadData;
    NSArray* subviews = [[NSArray alloc] initWithArray: self.leftView.subviews];
//    NSLog(@"subviews:%d",[subviews count]);
    for(UIImageView *view in subviews){
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }

    
    [self reloadDataSource];
    
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
//    [self loadJson];
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    return UITableViewCellEditingStyleDelete;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.relatesData.count;
}

// This will tell your UITableView what data to put in which cells in your table.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"%s:%d",__FUNCTION__,__LINE__);
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];

    NSDictionary *obj=[self.relatesData objectAtIndex:indexPath.row];
    
    NSURL *url = [NSURL URLWithString: [obj objectForKey:@"thumb"]];
    
    UIImage *thumbnail = [UIImage imageWithData: [NSData dataWithContentsOfURL:url]];
    if (thumbnail == nil) {
        thumbnail = [UIImage imageNamed:@"icon_No-Pic.jpg"] ;
    }
    CGSize itemSize = CGSizeMake(75, 75);
    UIGraphicsBeginImageContext(itemSize);
    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
    [thumbnail drawInRect:imageRect];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    cell.textLabel.text=[obj objectForKey:@"subject"];
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    sectionName=@"เนื้อหาที่เกี่ยวข้อง";
    return sectionName;
}
/******** Relate Table View *********/


@end
