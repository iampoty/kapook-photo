//
//  KPPhotoCollectionView.h
//  K@pook Photo
//
//  Created by Tharin Nilsri on 11/18/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"
#import <FacebookSDK/FacebookSDK.h>

@interface KPPhotoCollectionView : UIViewController <MWPhotoBrowserDelegate, UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource>
{
//    IBOutlet UITableView *relatesView;
//    IBOutlet UILabel *subjectLabel;
//    IBOutlet UICollectionView *collectionView;
//    NSDictionary *info;
    NSString *userid;
}

@property (nonatomic, strong) NSArray *contentArray;

@property (nonatomic, strong) NSMutableArray *listPhotos;

@property (nonatomic, retain) NSMutableArray *jsonData;
@property (nonatomic, retain) NSMutableArray *relatesData;
@property (nonatomic,strong) IBOutlet UILabel *subjectLabel;
@property (weak, nonatomic) IBOutlet FBLoginView *loginview;
@property (nonatomic,strong) IBOutlet UITableView *relatesView;
@property (strong, nonatomic) NSDictionary *info;

@property (nonatomic,retain) IBOutlet UIButton *tapRefreshBtn;

@property (nonatomic,strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) IBOutlet UIView *userinfoView;
@property (nonatomic,strong) IBOutlet UIImageView *userPicture;
@property (nonatomic,strong) IBOutlet UILabel *userLabel;
@property (nonatomic,strong) IBOutlet UILabel *viewsLabel;
@property (nonatomic,strong) IBOutlet UILabel *postdateLabel;
@property (nonatomic,strong) IBOutlet UIScrollView *scrollView;

//@property (nonatomic,strong) KPPhotoCollectionViewLayout_iPad *iPadLayout;

@end
