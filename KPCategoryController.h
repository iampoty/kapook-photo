//
//  KPCategoryController.h
//  K@pook Photo
//
//  Created by Tharin Nilsri on 10/30/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"
#import "AFNetworking.h"
#import "KPMainController.h"

@interface KPCategoryController : UITableViewController <UIScrollViewDelegate>{
//    NSMutableArray *listCategory;
    UIActivityIndicatorView *activityIndicator;
    UIButton *tapRefreshBtn;
    int tmpi;
}
@property (nonatomic, assign) MFSideMenuContainerViewController *sideMenu;
@property (strong, retain) NSMutableArray *listCategory;
//@property (nonatomic,strong) IBOutlet UIImageView *reloadBtn;
@property (nonatomic,strong) IBOutlet UIButton *tapRefreshBtn;
//@property (nonatomic,strong) UIView *view;
@property Boolean reloadCategory;
-(IBAction)reloadDataSource;
//    -(void) loadCategory;
//    -(void) reloadCategory;
@end
