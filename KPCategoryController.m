//
//  KPCategoryController.m
//  K@pook Photo
//
//  Created by Tharin Nilsri on 10/30/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import "KPCategoryController.h"
#import "MFSideMenu.h"
#import "KPMainController.h"

@interface KPCategoryController ()

@end

@implementation KPCategoryController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
//        CGFloat mainScreenHeight=[[UIScreen mainScreen] bounds].size.height;
//        NSLog(@"mainScreenHeight:%f",mainScreenHeight);
//        iOS 3.5" =480.000000
//        iOS 4" =568.000000
//        iPad 768.000000
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.reloadCategory=YES;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [self.tableView setContentInset:UIEdgeInsetsMake(22,
                                                         self.tableView.contentInset.left,
                                                         self.tableView.contentInset.bottom,
                                                         self.tableView.contentInset.right)];
    }
    
    if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPhone){
        CGFloat tbHeight=568;
        if([[UIScreen mainScreen] bounds].size.height<481){
            tbHeight=480;
        }
        self.tableView.frame=CGRectMake(0, 0, 320, tbHeight);
    }
    
    [self loadCategory];
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame=CGRectMake(0.0, 0.0, 250.0, 250.0);
    [self.view addSubview: activityIndicator];
//    [self.view bringSubviewToFront:activityIndicator];
//    [self.view bringSubviewToFront:self.tapRefreshBtn];
//    [self.view sendSubviewToBack:self.tapRefreshBtn];
    [activityIndicator startAnimating];
    
//    tapRefreshBtn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
//    [tapRefreshBtn setTitle:@"Tap to Refresh." forState:UIControlStateNormal];
//    [tapRefreshBtn setTitleColor: [UIColor redColor] forState: UIControlStateNormal];
//    [tapRefreshBtn.titleLabel setFont:[UIFont fontWithName:@"Verdana" size:12]];
//    [tapRefreshBtn setBackgroundColor:[UIColor clearColor]];
//    [tapRefreshBtn sizeThatFits:tapRefreshBtn.frame.size];
    
//    [self.view addSubview:tapRefreshBtn];
    
    self.refreshControl=nil;
    
    NSLog(@"viewDidLoad Cagegory");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadCategory{
    self.reloadCategory=YES;
    [self.listCategory removeAllObjects];
    self.listCategory=nil;
    self.listCategory=[[NSMutableArray alloc] init];
    
    [activityIndicator startAnimating];
    
    
    NSURL *URL = [NSURL URLWithString:@"http://kapi.kapook.com/world/category/list_content/photo"];
    
    if( tmpi % 2 == 0){
        URL = [NSURL URLWithString:@"http://kapi.kapook.com/world/category/list_content/photo"];
    }else{
        URL = [NSURL URLWithString:@"http://kapi.kapook.com/world/category/list_content/photo"];
    }
    
    NSLog(@"tmpi:%d URL:%@",tmpi,URL);
    
    NSURLRequest *request=[NSURLRequest requestWithURL:URL];
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        self.listCategory = [responseObject objectForKey:@"data"];
        [self dataSourceDidLoad];
        [self.tableView setHidden:NO];
        [self.tableView reloadData];
        self.reloadCategory=NO;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.reloadCategory=NO;
        NSLog(@"Error: %@", error);
        [self dataSourceDidError];
    }];
    [[NSOperationQueue mainQueue] addOperation:op];
//    [op start];
}

-(IBAction)reloadDataSource{
    [self loadCategory];
    tmpi++;
}


-(void)dataSourceDidLoad{
    NSLog(@"dataSourecDidload");
    [activityIndicator stopAnimating];
}


-(void)dataSourceDidError{
    NSLog(@"dataSourecDidError");
    
    self.listCategory=nil;
    
    [activityIndicator stopAnimating];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    NSLog(@"%s",__FUNCTION__);
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSLog(@"%s",__FUNCTION__);
//    NSLog(@"%s / %d",__FUNCTION__,self.listCategory.count);
    return self.listCategory.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"indexPath.row:%ld",(long)indexPath.row);
    static NSString *CellIdentifier = @"Cell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell==nil){
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    NSDictionary *obj=[self.listCategory objectAtIndex:indexPath.row];
    cell.textLabel.text=[obj objectForKey:@"name"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    KPMainController *mainController=nil;
    if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPhone){        
        mainController=[[KPMainController alloc] initWithNibName:@"KPMainController~iPhone" bundle:nil];
    }else{
        mainController=[[KPMainController alloc] initWithNibName:@"KPMainController~iPad" bundle:nil];
    }
    NSDictionary *obj = [self.listCategory objectAtIndex:indexPath.row];
    mainController.title = [obj objectForKey:@"name"];
    mainController.section = [obj objectForKey:@"id"];

    
    UINavigationController *navigationController=self.menuContainerViewController.centerViewController;
    NSArray *controllers=[NSArray arrayWithObjects:mainController, nil];
    navigationController.viewControllers=controllers;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.listCategory == nil && self.reloadCategory==NO) {
            NSLog(@"%s:%d Reload",__FUNCTION__,__LINE__);
        self.reloadCategory=YES;
        [self reloadDataSource];
    }

}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"%s:%d Reload",__FUNCTION__,__LINE__);
}
-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"%s:%d Reload",__FUNCTION__,__LINE__);
}
@end
