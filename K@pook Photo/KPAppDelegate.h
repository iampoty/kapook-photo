//
//  KPAppDelegate.h
//  K@pook Photo
//
//  Created by Tharin Nilsri on 10/30/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "KPMainController.h"
#import "KPCategoryController.h"
#import "KPPhotoCollectionView.h"
@interface KPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
//@property (strong,nonatomic) KPMainController *mainController;
@property (strong,nonatomic) FBSession *session;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
