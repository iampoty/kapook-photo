//
//  KPAppDelegate.m
//  K@pook Photo
//
//  Created by Tharin Nilsri on 10/30/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import "KPAppDelegate.h"
#import "MFSideMenu.h"

@implementation KPAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

-(KPMainController *)mainController{
    if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPhone){
        return [[KPMainController alloc] initWithNibName:@"KPMainController~iPhone" bundle:nil];
    }else{
        return [[KPMainController alloc] initWithNibName:@"KPMainController~iPad" bundle:nil];
    }
}

-(KPCategoryController *)categoryController{
    if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPhone){
        NSLog(@"KPCategoryController~iPhone");
        return [[KPCategoryController alloc] initWithNibName:@"KPCategoryController~iPhone" bundle:nil];
    }else{
        NSLog(@"KPCategoryController~iPad");
//        return [[KPCategoryController alloc] initWithNibName:@"KPCategoryController~iPhone" bundle:nil];
        return [[KPCategoryController alloc] initWithNibName:@"KPCategoryController~iPad" bundle:nil];
    }
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"KPAppDelegate  > %s:%d",__FUNCTION__,__LINE__);
    [FBLoginView class];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UINavigationController *navigationController=[[UINavigationController alloc] initWithRootViewController:[self mainController]];
    
    MFSideMenuContainerViewController *container=[MFSideMenuContainerViewController
                                                  containerWithCenterViewController:navigationController
                                                  leftMenuViewController:[self categoryController]
                                                  rightMenuViewController:nil];
    
    
//    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
//    if ([[ver objectAtIndex:0] intValue] >= 7) {
//        navigationController.navigationBar.backgroundColor=[[UIColor alloc] initWithRed:7.0/255 green:163.0/255 blue:116.0/255 alpha:1.0];
//        navigationController.navigationBar.translucent=NO;
//    }else{
//        navigationController.navigationBar.tintColor=[[UIColor alloc] initWithRed:7.0/255 green:163.0/255 blue:116.0/255 alpha:1.0];
//    }
    
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        navigationController.navigationBar.backgroundColor=[[UIColor alloc] initWithRed:7.0/255 green:163.0/255 blue:116.0/255 alpha:1.0];
    }else{
        navigationController.navigationBar.tintColor=[[UIColor alloc] initWithRed:7.0/255 green:163.0/255 blue:116.0/255 alpha:1.0];
    }
    
    self.window.rootViewController=container;
    self.window.backgroundColor = [UIColor whiteColor];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBAppCall handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [self saveContext];
    [FBSession.activeSession close];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"K_pook_Photo" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"K_pook_Photo.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}
@end
