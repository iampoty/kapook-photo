//
//  KPMainController.m
//  K@pook Photo
//
//  Created by Tharin Nilsri on 10/30/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//
#import "MFSideMenu.h"
#import "KPMainController.h"
//#import "PSCollectionView.h"
#import "KPPhotoViewCell.h"
#import "KPPhotoCollectionView.h"
#import <QuartzCore/QuartzCore.h>

static BOOL isDeviceIPad() {
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    }
#endif
    return NO;
}

@interface KPMainController (){
    UIActivityIndicatorView *activityIndicator;
}
@property (nonatomic,strong) NSMutableArray *collectionViewData;
@end

@implementation KPMainController
@synthesize listPhotos =listPhotos;
@synthesize collectionView;
@synthesize section;
@synthesize tapRefresh;
@synthesize tapRefreshBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    _reloading=YES;
	float h = 50;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        
        self.edgesForExtendedLayout = UIRectEdgeNone;

    }
    
    if(!self.title) self.title = @"K@pook! Photo";    
    [self setupMenuBarButtonItems];
    
    [self.listPhotos removeAllObjects];
    self.listPhotos = nil;
    self.listPhotos = [[NSMutableArray alloc] init];

    
    NSURL *websiteUrl = [NSURL URLWithString:@"http://iosad.kapook.com"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [_webview loadRequest:urlRequest];
    
    self.collectionView =[[PSCollectionView alloc] initWithFrame:CGRectMake(0, h, self.view.frame.size.width, self.view.frame.size.height)];
    
    
    self.collectionView.delegate=self;
    self.collectionView.collectionViewDelegate=self;
    self.collectionView.collectionViewDataSource=self;
    self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    self.collectionView.backgroundColor=[UIColor whiteColor];

    if (isDeviceIPad()) {
        self.collectionView.numColsPortrait = 3;
        self.collectionView.numColsLandscape = 3;
    } else {
        self.collectionView.numColsPortrait = 1;
        self.collectionView.numColsLandscape = 2;
    }
    [self.view addSubview:collectionView];

    [self loadDataSource];
    
//    self.collectionView.hidden=YES;
    self.tapRefreshBtn.hidden=NO;
    self.tapRefreshBtn.userInteractionEnabled=YES;
    
/**** Pull to Refresh *****/
    UIRefreshControl *refreshControl=[[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull to Refresh"];
    [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
    self.collectionView.alwaysBounceVertical=YES;
/**** Pull to Refresh *****/
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame=CGRectMake(0.0, 0.0, 0.0, 0.0);
    activityIndicator.transform = CGAffineTransformMakeScale(2.0, 2.0);
    activityIndicator.center = self.view.center;
    [self.view addSubview: activityIndicator];
    [activityIndicator startAnimating];
    
}

/**** Pull to Refresh *****/
-(void)handleRefresh:(UIRefreshControl *) refreshControl{
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing data..."];
    page=1;
    [self loadDataSource];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm:ss a"];
    NSString *lastUpdate = [NSString stringWithFormat:@"Last Update On %@", [formatter stringFromDate:[NSDate date]]];
    
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:lastUpdate];
    [refreshControl endRefreshing];
}
/**** Pull to Refresh *****/

/***** Load More ****/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView contentOffset].y > [scrollView contentSize].height - [scrollView frame].size.height && _reloading==NO) {
        _reloading=YES;
        page = page+1;
        [self loadDataSource];
    }
}
/***** Load More ****/

-(void)loadDataSource{
    
    if(section==nil){
        section=@"0";
    }
    if(page<1){
        page=1;
    }

    [activityIndicator startAnimating];
    NSString *url = [NSString stringWithFormat:@"http://202.183.165.18/world/content/list_content/images/%@/%d/30",section,page];
    NSLog(@"url %@",url);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSArray *photos = [responseObject objectForKey:@"data"];
        
        NSString *lastid=@"";
        NSString *newid=@"";

        if(self.listPhotos.count>0  && page==1) {
            NSDictionary *p=[self.listPhotos objectAtIndex:0];
            lastid=[NSString stringWithFormat:@"[%@]",[p objectForKey:@"id"]];
            newid=[NSString stringWithFormat:@"[%@]",[photos[0] objectForKey:@"id"]];
            
            if(![newid isEqualToString:lastid]){
                int i;
                int c=0;
                for(i=0;i<photos.count;i++)
                {
                    newid=[photos[i] objectForKey:@"id"];
                    
                    if([newid isEqualToString:lastid]){
//                        NSLog(@"lastid=%@ , newid=%@ break",lastid,newid);
                        break;
                        break;
                    }else{
//                        NSLog(@"lastid=%@ , insert newid=%@",lastid,newid);
                        [self.listPhotos insertObject:photos[i] atIndex:c];
                        c++;
                    }
                }
            }else{
//                NSLog(@"lastid=%@ == newid=%@",lastid,newid);
            }
        }else{
            int i;
            for(i=0;i<photos.count;i++)
            {
                [self.listPhotos addObject:photos[i]];
            }
//            NSLog(@"init listPhots page %d success.",page);
        }
//        self.listPhotos = [responseObject objectForKey:@"data"];
        [self dataSourceDidLoad];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self dataSourceDidError];
    }];
    
}

-(void)dataSourceDidLoad{
//    NSLog(@"finishLoadData");
    [collectionView reloadData];
    [activityIndicator stopAnimating];
    _reloading=NO;
}

-(void)dataSourceDidError{
    [activityIndicator stopAnimating];
//    self.collectionView.hidden=YES;
//    self.tapRefresh.hidden=YES;
//    self.tapRefreshBtn.hidden=NO;
    [self.view bringSubviewToFront:self.tapRefreshBtn];
}

/**** MFSideMenu******/
- (void)setupMenuBarButtonItems {
    if(self.navigationItem.rightBarButtonItem!=nil){
        self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    }
    if(self.menuContainerViewController.menuState == MFSideMenuStateClosed &&
       ![[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
        self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
    } else {
        self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
    }
}


- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(leftSideMenuButtonPressed:)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(rightSideMenuButtonPressed:)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)leftSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        NSLog(@"click Left");
        [self setupMenuBarButtonItems];
    }];
}

- (void)rightSideMenuButtonPressed:(id)sender {
    [self.menuContainerViewController toggleRightSideMenuCompletion:^{
        [self setupMenuBarButtonItems];
    }];
}

- (IBAction)pushAnotherPressed:(id)sender {
}
/**** MFSideMenu******/

/********* PSCollectionView*******/

- (NSInteger)numberOfRowsInCollectionView:(PSCollectionView *)collectionView {
    return [self.listPhotos count];
}

- (PSCollectionViewCell *)collectionView:(PSCollectionView *)collectionView cellForRowAtIndex:(NSInteger)index {
    KPPhotoViewCell *v=(KPPhotoViewCell * )[self.collectionView dequeueReusableViewForClass:nil];
    if(!v){
        v=[[KPPhotoViewCell alloc] initWithFrame:CGRectZero];
    }
    
    [v collectionView:self.collectionView fillCellWithObject:self.listPhotos atIndex:index];
    return v;
}

- (CGFloat)collectionView:(PSCollectionView *)collectionView heightForRowAtIndex:(NSInteger)index {
    NSDictionary *item = [self.listPhotos objectAtIndex:index];
    return [KPPhotoViewCell heightForViewWithObject:item inColumnWidth:self.collectionView.colWidth];
}


- (void)collectionView:(PSCollectionView *)collectionView didSelectCell:(PSCollectionViewCell *)cell atIndex:(NSInteger)index {
//    KPPhotoViewController *p=nil;
//    if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPhone){
//        p =[[KPPhotoViewController alloc] initWithNibName:@"KPPhotoViewController~iPhone" bundle:nil];
//    }else{
//        p =[[KPPhotoViewController alloc] initWithNibName:@"KPPhotoViewController~iPad" bundle:nil];
//    }
//    p.info = [self.listPhotos objectAtIndex:index];
//    [self.navigationController pushViewController:p animated:YES];
    

    KPPhotoCollectionView *p=nil;
    if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPhone){
        if([[UIScreen mainScreen] bounds].size.height<481){
            p =[[KPPhotoCollectionView alloc] initWithNibName:@"KPPhotoCollectionView~iPhone~3.5" bundle:nil];
        }else{
            p =[[KPPhotoCollectionView alloc] initWithNibName:@"KPPhotoCollectionView~iPhone" bundle:nil];
        }
    }else{
        p =[[KPPhotoCollectionView alloc] initWithNibName:@"KPPhotoCollectionView~iPad" bundle:nil];
    }
    p.info = [self.listPhotos objectAtIndex:index];
    [self.navigationController pushViewController:p animated:YES];

    
}

- (void)collectionView:(PSCollectionView *)collectionView didSelectView:(PSCollectionViewCell *)view atIndex:(NSInteger)index {
//    NSLog(@"%s / %d",__FUNCTION__,__LINE__);
}
/********* PSCollectionView*******/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/***** Tap to Refresh ******/
-(IBAction)reloadDataSource{
    [activityIndicator startAnimating];
    [self.view sendSubviewToBack:self.tapRefreshBtn];
    [self loadDataSource];
}
@end
