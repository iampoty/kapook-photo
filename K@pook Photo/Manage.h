//
//  Manage.h
//  Kapook!w
//
//  Created by mookapook on 10/24/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "MBProgressHUD.h"
//#import "FirstView.h"
#import <FacebookSDK/FacebookSDK.h>
@class FirstView;
@interface Manage : NSObject<MBProgressHUDDelegate>
@property (nonatomic, retain) NSMutableArray *movies;
@property (strong, nonatomic) MBProgressHUD *HUD;
@property (strong, nonatomic) MBProgressHUD *HUD2;
+ (id)sharedManager;
- (NSArray *)getMenu;
-(NSMutableArray *)loadFirst:(NSDictionary*) _info andtable:(UITableView *)_tableview initWithView:(UIView *)view;

-(void)noti;
-(void) profileFacebook;
- (NSArray *)readprofile;
-(void)AddtoKapook:(NSDictionary*) info initWithView:(UIView *)view;
@end
