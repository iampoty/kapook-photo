//
//  Manage.m
//  Kapook!w
//
//  Created by mookapook on 10/24/56 BE.
//  Copyright (c) 2556 mookapook. All rights reserved.
//

#import "Manage.h"

@implementation Manage
+ (id)sharedManager {
    static Manage *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        //someProperty = [[NSString alloc] initWithString:@"Default Property Value"];
    }
    return self;
}



- (NSArray *)readprofile
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"User.plist"];
    
    // Build the array from the plist
    NSArray *array2 = [[NSArray alloc] initWithContentsOfFile:filePath];
    NSDictionary *m = [array2 objectAtIndex:0];
    
    NSLog(@"%@",[m objectForKey:@"name"]);
        
    // Show the string values
    for (NSString *str in array2)
        NSLog(@"--%@", str);
    //NSString *value = [plistDict objectForKey:@"lang"];
    return array2;
}

-(void)AddtoKapook:(NSDictionary*) info initWithView:(UIView *)view{
    
    
    
    //[self.HUD setHidden:NO];
    
    
    NSArray *profile = [self readprofile];
    
    NSDictionary *m = [profile objectAtIndex:0];
    
    NSLog(@"%@ : userid",[m objectForKey:@"userid"]);
    
    
    
    if(!self.HUD)
    {
        NSLog(@"LOAD HUD");
        
    self.HUD = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:self.HUD];
	
    
    [self.HUD show:YES];
    
    }
    else
    {
        NSLog(@"NOT LOAD HUD");
        [self.HUD show:YES];
        self.HUD.labelText = @"Kapook! World Clip";
        self.HUD.detailsLabelText = @"กำลังส่งข้อมูล";
        self.HUD.square = YES;
        
    }
    
    NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
    
    NSString *uid = [m objectForKey:@"userid"];
    
    
    [cookieProperties setObject:@"uidc" forKey:NSHTTPCookieName];
    [cookieProperties setObject:uid forKey:NSHTTPCookieValue];
    [cookieProperties setObject:@".kapook.com" forKey:NSHTTPCookieDomain];    // Without http://
    [cookieProperties setObject:@".kapook.com" forKey:NSHTTPCookieOriginURL]; // Without http://
    [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
    
    // set expiration to one month from now or any NSDate of your choosing
    // this makes the cookie sessionless and it will persist across web sessions and app launches
    /// if you want the cookie to be destroyed when your app exits, don't set this
    [cookieProperties setObject:[NSDate dateWithTimeIntervalSinceNow:3600*24]  forKey:NSHTTPCookieExpires];
    
    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
    
    
    
//    NSString *urlPost = @"http://beta.market.kapook.com";
//    NSString *urlPostApi = @"http://beta.market.kapook.com/add2.kapook/api.php";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"uid":[m objectForKey:@"userid"],@"url":[info objectForKey:@"link"],@"from":@"iOS"};
    NSLog(@"parameters:%@",parameters);
//    NSString *url=@"http://signup-demo.kapook.com/add2kapook.php";
    NSString *url=@"http://beta.market.kapook.com/add2.kapook/api.php";
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
//              for (NSHTTPCookie *cookie in cookies) {
//                  NSLog(@"%@",cookies);
//                }

        [self showAlert:@"จัดเก็บเรียบร้อยแล้ว"];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self showAlert:@"ไม่สามารถจัดเก็บได้"];
    }];
    [self.HUD hide:YES afterDelay:0.5];
    
}

- (void)showAlert:(NSString *) alertMsg {
    if (![alertMsg isEqualToString:@""]) {
        [[[UIAlertView alloc] initWithTitle:@"Result"
                                    message:alertMsg
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}

-(void)profileFacebook
{
    
    //id a;
    __block id<FBGraphUser> users ;
    if(FBSession.activeSession.isOpen)
    {
       [FBRequestConnection
         startForMeWithCompletionHandler:^(FBRequestConnection *connection,
                                           id<FBGraphUser> user,
                                           NSError *error) {
             if (!error) {
                 NSString *userInfo = @"";
                 
                 // Example: typed access (name)
                 // - no special permissions required
                 userInfo = [userInfo
                             stringByAppendingString:
                             [NSString stringWithFormat:@"Name: %@\n\n",
                              user.name]];
                 
                 // Example: typed access, (birthday)
                 // - requires user_birthday permission
                 userInfo = [userInfo
                             stringByAppendingString:
                             [NSString stringWithFormat:@"Birthday: %@\n\n",
                              user.birthday]];
                 
                 // Example: partially typed access, to location field,
                 // name key (location)
                 // - requires user_location permission
                 userInfo = [userInfo
                             stringByAppendingString:
                             [NSString stringWithFormat:@"Location: %@\n\n",
                              user.location[@"name"]]];
                 
                 // Example: access via key (locale)
                 // - no special permissions required
                 userInfo = [userInfo
                             stringByAppendingString:
                             [NSString stringWithFormat:@"Locale: %@\n\n",
                              user[@"locale"]]];
                 
                 // Example: access via key for array (languages)
                 // - requires user_likes permission
                 if (user[@"languages"]) {
                     NSArray *languages = user[@"languages"];
                     NSMutableArray *languageNames = [[NSMutableArray alloc] init];
                     for (int i = 0; i < [languages count]; i++) {
                         languageNames[i] = languages[i][@"name"];
                     }   
                     userInfo = [userInfo
                                 stringByAppendingString:
                                 [NSString stringWithFormat:@"Languages: %@\n\n",
                                  languageNames]];
                 }   
                 
                 // Display the user info
                 //self.userInfoTextView.text = userInfo;
                 NSLog(@"%@",user);
                 users = user;
                 //return user;
             }   
         }];
    
    }
    
    
}

-(NSArray *)getMenu
{
    NSArray *a = @[@"before"];
    return a;
}

-(void)noti
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"loadtable"
                                                  object:nil];
    NSLog(@"LOAD NOTI");
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadtable"
                                                        object:nil];
    
}


//-(NSMutableArray *)loadFirst:(NSDictionary*) _info andtable:(UITableView *)_tableview initWithView:(UIView *)view{
//    
//    self.HUD = [[MBProgressHUD alloc] initWithView:view];
//    [view addSubview:self.HUD];
//	
//    self.HUD.delegate = self;
//    self.HUD.labelText = @"Kapook! World Clip";
//    self.HUD.detailsLabelText = @"กำลังโหลดข้อมูล";
//	self.HUD.square = YES;
//	[self.HUD show:true];
//    
//    
//    
//    NSString *u;
//    
//    NSLog(@"%@",[_info objectForKey:@"id"]);
//    
//    _movies = [[NSMutableArray alloc] init];
//    u = [NSString stringWithFormat:@"http://kapi.kapook.com/world/content/get_content/%@",[_info objectForKey:@"id"]];
//    
//    NSURL *url = [[NSURL alloc] initWithString:u];
//    //_movies = [[NSMutableArray alloc] init];
//    
//    //AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
//    
//    //    AFNetworkActivityIndicatorManager * newactivity = [[AFNetworkActivityIndicatorManager alloc] init];
//    //    newactivity.enabled = YES;
//    //
//    //NSString *d = @"0";
//    
//    //int aValue = [_lastId intValue];
//    //    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
//    //                            lastId, @"latest_id",
//    //                            @"777", @"page_id",
//    //                            @"1", @"owner",
//    //                            nil];
//    //    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:@"profile/feedLoadMore" parameters:params];
//    
//    //NSURLRequest *request2 = [[NSURLRequest alloc] initWithURL:url];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    
//    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
//        //self.movies = [JSON objectForKey:@"data"];
//        
//        // NSLog(@"xxx %@",JSON);
//        NSArray *m = [[NSArray alloc] init];
//        
//        NSNumber *isSeries = [JSON objectForKey:@"isSeries"];
//        
//        NSLog(@"%d",[isSeries integerValue]);
//        
//        if([isSeries integerValue] > 0)
//        {
//            m = [JSON objectForKey:@"contents"];
//        }
//        else
//        {
//            m = [JSON objectForKey:@"relates"];
//        }
//        NSLog(@"xx : %d",m.count);
//        int i;
//        for(i=0;i<m.count;i++)
//        {
//            //NSLog(@"%@",m[i]);
//            [_movies addObject:m[i]];
//            
//        }
//        
//        NSLog(@"c: %d",_movies.count);
//        
//        //numberContent = i;
//        //[self.movies addObject:[JSON objectForKey:@"data"]];
//        
//        //[self.activityIndicatorView stopAnimating];
//        //[_tableView setHidden:NO];
//        [_tableview reloadData];
//        [self.HUD setHidden:YES];
//
//        //
//    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
//        NSLog(@"Request Failed with Error: %@, %@", error, error.userInfo);
//        [self.HUD setHidden:YES];
//        
//        UIAlertView *tmp = [[UIAlertView alloc]
//                            initWithTitle:@"Error"
//                            message:@"Unable to Load the photo please try later."
//                            delegate:self
//                            cancelButtonTitle:nil
//                            otherButtonTitles:@"Ok", nil];
//        
//        [tmp show];
//        
//        
//    }];
//    
//    //_tableView.layer.cornerRadius = 10;
//    
//    [operation start];
//    
//    return _movies;
//}




- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

@end
