//
//  KPMainController.h
//  K@pook Photo
//
//  Created by Tharin Nilsri on 10/30/56 BE.
//  Copyright (c) 2556 Kapook.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSCollectionView.h"
//#import "KPAppDelegate.h"
#import "AFNetworking.h"

@interface KPMainController : UIViewController <PSCollectionViewDelegate,PSCollectionViewDataSource,UIScrollViewDelegate>{
    BOOL _reloading;
    int page;
//    NSString *section;
    PSCollectionView *collectionView;
}
-(IBAction)reloadDataSource;
@property (nonatomic,retain) IBOutlet UILabel *tapRefresh;
@property (nonatomic,retain) IBOutlet UIButton *tapRefreshBtn;
@property (nonatomic,strong) IBOutlet UIView *view;
@property (nonatomic,weak) IBOutlet UIWebView *webview;
@property (strong, nonatomic) NSString *section;
@property (strong, retain) NSMutableArray *listPhotos;
@property (nonatomic, retain) PSCollectionView *collectionView;
@end
